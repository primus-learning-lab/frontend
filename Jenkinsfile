pipeline {
    agent any
    tools{
        maven 'maven'
    }
    stages {
        stage('Build') {
            steps {
                // Build the Maven app
                echo "$env.BRANCH_NAME , $env.CHANGE_TITLE, $env.CHANGE_TARGET, $env.CHANGE_BRANCH"
                sh 'mvn clean package'
                archiveArtifacts artifacts: '**/*.jar'
            }
        } 

        stage('Unit Test') {
            steps {
                // Run JUnit tests
                sh 'mvn test'
            }
        }

        stage('Integration Test'){
            steps {
                sh 'mvn verify -DskipUnitTests'
            }
        }

        
        stage('SonarQube scan') {
            steps {
                // Run SonarQube scan
        withSonarQubeEnv(installationName: 'sonar-scanner') { 
          sh 'mvn  org.sonarsource.scanner.maven:sonar-maven-plugin:3.9.0.2155:sonar'
        }
            }
        }

        stage("Quality Gate"){
            steps{
                script{
                    timeout(time: 15, unit: 'SECONDS') {
                        def qg = waitForQualityGate() 
                        if (qg.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                        }
                    }
                }
            }
        }


        stage('Get app version And deploy to Nexus') {
            steps {
                script{
                    def pom = readMavenPom file: 'pom.xml'
                    def appVersion = pom.version
                    if (env.BRANCH_NAME != 'master') {
                        if(env.BRANCH_NAME.startsWith("feature/")){
                            brancheName= env.BRANCH_NAME.replace("/","-")
                        } else{
                            brancheName= env.BRANCH_NAME
                        }
                        appVersion += "-${brancheName}-${env.BUILD_NUMBER}"
                    }

                    echo "App version: ${appVersion}"
                    sh "ls -l"
                    nexusArtifactUploader(
                    nexusVersion: 'nexus3',
                    protocol: 'http',
                    nexusUrl: '18.218.64.132:8081',
                    groupId: 'com.example',
                    version:  "${appVersion}",
                    repository: 'maven-releases',
                    credentialsId: 'nexus',
                    artifacts: [
                        [artifactId: pom.artifactId,
                        classifier: '',
                        file: "${WORKSPACE}/target/frontend-1.1.0-SNAPSHOT.jar",
                        type: 'jar']
                    ]
                )
                }
            }
        }

        stage('accept merge Request') {
            when {
                changeRequest target: 'main'
            }
            steps {
                script{
                    updateGitlabCommitStatus name: 'jenkinsci/mr-head', state: 'failed'
                }
            }
        }


        stage('deploy to dev') {
            when {
                branch 'main'
            }
            environment {
                HOSTS = "dev"
            }
            steps {
                    sh " ls -l "
                    sh "ansible-playbook playbook.yaml -b -u ubuntu --extra-vars \"hosts=$HOSTS workspace_path=$WORKSPACE\""
            }
        }

            
        stage('Deploy to Stage') {
            when {
                branch 'master'
            }
            environment {
                HOSTS = "stage" // Make sure to update to "stage"
            }
            steps {
                sh "ansible-playbook ${WORKSPACE}/playbook.yaml --extra-vars \"hosts=$HOSTS workspace_path=$WORKSPACE\""

            }
        }
        
        stage('Approval') {
             when {
                branch 'master'
            }
            steps {
                input('Do you want to proceed?')
            }
        }

        
        stage('Deploy to PROD') {
            when {
                branch 'master'
            }
            environment {
                HOSTS = "prod"
            }
            steps {
                sh "ansible-playbook ${WORKSPACE}/playbook.yaml --extra-vars \"hosts=$HOSTS workspace_path=$WORKSPACE\""
            }
        }

    }

    post {
        always {
            emailext body: 'Test Message',
            recipientProviders: [developers(), requestor()],
            subject: 'Test Subject',
            to: 'azizabdulaa540@gmail.com'
        }
    }
 
}
